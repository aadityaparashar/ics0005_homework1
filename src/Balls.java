
public class Balls {

   enum Color {green, red};
   
   public static void main (String[] param) {
      // for debugging
   		Color[] balls = {Color.green,Color.red,Color.red,Color.green,Color.red,Color.red,Color.green,Color.red,Color.red};
   		reorder(balls);
   }
   
   public static void reorder (Color[] balls) {
      // TODO!!! Your program here
   		int num_green = 0, num_red = 0;

   		for (int i = 0; i < balls.length; i++){

   			if (balls[i] == Color.green){
   				num_green += 1;
   			}

   			if (balls[i] == Color.red){
   				num_red += 1;
   			}
   		}
   		//System.out.println(""+num_green +" "+ num_red);
   		Color[] balls_new = new Color[balls.length];
   		int i = 0;
   		for (i = 0; i<num_red; i++){
   			balls_new[i] = Color.red;
   		}

   		for (int j = i; j < balls.length; j++){
   			balls_new[j] = Color.green;
   		}
   		
   		balls = balls_new;

   		/*for (int z = 0; z < balls.length; z++) {
            System.out.print("" + balls[z]+ "\n" );
        }*/
   }
}

